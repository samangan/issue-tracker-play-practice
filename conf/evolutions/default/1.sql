

# --- !Ups

CREATE TABLE developer (
  id      SERIAL PRIMARY KEY,
  name    VARCHAR(255) NOT NULL
);

CREATE TABLE issue (
  id    SERIAL PRIMARY KEY,
  dateCreated   TIMESTAMP NOT NULL DEFAULT CURRENT_DATE,
  title     VARCHAR(255) NOT NULL,
  description   TEXT NOT NULL,
  developerAssigned   INTEGER NOT NULL REFERENCES developer(id)
);

CREATE TABLE comment (
  id      SERIAL  PRIMARY KEY,
  dateCreated    TIMESTAMP NOT NULL DEFAULT CURRENT_DATE,
  content TEXT NOT NULL,
  issue   INTEGER NOT NULL REFERENCES issue(id)
);



# --- !Downs

DROP TABLE IF EXISTS issue;
DROP TABLE IF EXISTS comment;
DROP TABLE IF EXISTS developer;