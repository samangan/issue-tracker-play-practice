import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "codeReviewIssueTracker"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    "postgresql" % "postgresql" % "9.1-901-1.jdbc4",
    "com.cloudphysics" % "jerkson_2.10" % "0.6.3",
    jdbc,
    anorm
  )


  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here
  )

}
