package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import models._
import anorm.NotAssigned
import java.util.{Date}
import com.codahale.jerkson.Json


object Application extends Controller {
   val issueForm = Form(
    tuple(
      "title" -> nonEmptyText,
      "description" -> nonEmptyText,
      "developerAssigned" -> number
    )
  )

  val developerForm = Form(
    single("name" -> nonEmptyText)
  )

  def index = Action {
    Ok(views.html.index(issueForm, developerForm, Issue.all, Developer.all))
  }

  def getIssue(id: Long) = Action {
    Ok(views.html.issue(Issue.getIssueByID(id).get))
  }

  def issues = TODO

  def newIssue() = Action { implicit request =>
    issueForm.bindFromRequest.fold(
    errors => BadRequest(views.html.index(errors, developerForm, Issue.all, Developer.all)),
    {
      case (title, description, developerAssigned) =>
        Issue.create(Issue(NotAssigned, Option(new Date) , title, description, developerAssigned))
        Redirect(routes.Application.index())
    }
    )
  }

  def newDeveloper() = Action { implicit request =>
    developerForm.bindFromRequest.fold(
    errors => BadRequest(views.html.index(issueForm, errors, Issue.all, Developer.all)),
    {
      case (name) =>
        Developer.create(Developer(NotAssigned, name))
        Redirect(routes.Application.index())
    }
    )
  }

  def developers() = Action { implicit request =>
    val developers = Developer.all
    val json = Json.generate(developers)
    Ok(json).as("application/json")
  }




}