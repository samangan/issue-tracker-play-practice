package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import java.util.{Date}
import play.api.Play.current

case class Issue(id: Pk[Long], dateCreated: Option[Date], title: String, description: String, developerAssigned: Int)

object Issue {

  val simple = {
    get[Pk[Long]]("id") ~
    get[Option[Date]]("dateCreated") ~
    get[String]("title") ~
    get[String]("description") ~
    get[Int]("developerAssigned") map {
      case id ~ dateCreated ~ title ~ description ~ developersAssigned => Issue(id, dateCreated, title, description, developersAssigned)
    }
  }

  def all(): Seq[Issue] = {
    DB.withConnection{ implicit c =>
      SQL("select * from issue").as(Issue.simple * )
    }
  }

  def create(issue: Issue): Unit = {
    DB.withConnection{ implicit c =>
      SQL("insert into issue (dateCreated, title, description, developerAssigned) values ({dateCreated}, {title}, {description}, {developerAssigned})").on(
        'dateCreated -> issue.dateCreated,
        'title -> issue.title,
        'description -> issue.description,
        'developerAssigned -> issue.developerAssigned
      ).executeUpdate()
    }
  }


  def delete(issue: Issue) {
    DB.withConnection{ implicit c =>
       SQL("delete from issue where id={id}").on(
        'id -> issue.id
       ).executeUpdate()
    }
  }

  def getIssueByID(id: Long): Option[Issue] = {
    Issue.all().find((i: Issue) => i.id == id)
  }
}
