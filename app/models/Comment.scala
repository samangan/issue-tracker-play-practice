package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import java.util.{Date}
import play.api.Play.current

case class Comment (id: Pk[Long], dateCreated: Option[Date], content: String, issue: Option[Int])

object Comment {

  val simple = {
    get[Pk[Long]]("id") ~
    get[Option[Date]]("dateCreated") ~
    get[String]("content") ~
    get[Option[Int]]("issue") map {
      case id ~ dateCreated ~ content ~ issue => Comment(id, dateCreated, content, issue)
    }
  }

  def all(): Seq[Comment] = {
    DB.withConnection{ implicit  connection =>
      SQL("select * from comments").as(Comment.simple *)
    }
  }

  def findAllForIssue(issue: Issue): Seq[Comment] = {
    DB.withConnection{ implicit connection =>
      SQL("select * from comment where issue = {issue} ").on(
        'issue -> issue.id
      ).as(Comment.simple *)
    }
  }


  def delete(comment: Comment) {
    DB.withConnection{ implicit connection =>
      SQL("delete from comment where id = {id}").on(
        'id -> comment.id
      ).executeUpdate()
    }
  }
}
