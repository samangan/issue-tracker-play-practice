package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class Developer (id: Pk[Long], name: String)

object Developer {

  val simple = {
    get[Pk[Long]]("id") ~
    get[String]("name") map {
      case id ~ name => Developer(id, name)
    }
  }

  def all(): Seq[Developer] = {
    DB.withConnection { implicit  connection =>
      SQL("select * from developer").as(Developer.simple * )
    }
  }

    def create(dev: Developer): Unit = {
      DB.withConnection{ implicit connection =>
        SQL("insert into developer(name) values ({name})").on(
          'name -> dev.name
        ).executeUpdate()
      }
  }

  def delete(dev: Developer) {
    DB.withConnection( implicit connection =>
      SQL("delete from developer where id = {id}").on(
        'id -> dev.id
      ).executeUpdate()
    )
  }

  def getDevByID(id: Long): Option[Developer] = {
     Developer.all().find((dev: Developer) => dev.id.toString.toLong == id)
  }





}
